package com.artrest.artrest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    EditText editTextUsername, editTextPassword;
    Button buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editTextUsername = (EditText)findViewById(R.id.editTextUsername);
        editTextPassword = (EditText)findViewById(R.id.editTextPassword);
        buttonLogin = (Button) findViewById(R.id.buttonLogin);

        buttonLogin.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                String username = editTextUsername.getText().toString();
                String password = editTextPassword.getText().toString();

                if (username.trim().equalsIgnoreCase("")) {
                    editTextUsername.setError("username tidak boleh kosong");
                    editTextUsername.requestFocus();
                } else if (password.trim().equalsIgnoreCase("")) {
                    editTextPassword.setError("Password tidak boleh kosong");
                    editTextPassword.requestFocus();
                } else if (username.equalsIgnoreCase("user123") && password.equalsIgnoreCase("user123")) {
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    Toast.makeText(LoginActivity.this, "Username dan password tidak sesuai", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
