package com.artrest.artrest.fragment;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.artrest.artrest.LoginActivity;
import com.artrest.artrest.R;
import com.artrest.artrest.UploadArtworkActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class TimelineFragment extends Fragment {

    FloatingActionButton btnaddart;

    public TimelineFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_timeline, container, false);
        btnaddart = (FloatingActionButton) view.findViewById(R.id.floatingActionButton2);
        return view;
    }

    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);
        btnaddart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), UploadArtworkActivity.class);
                startActivity(intent);
                getActivity().finish();
            }
        });
    }



}
