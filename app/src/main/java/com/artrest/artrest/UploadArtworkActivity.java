package com.artrest.artrest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class UploadArtworkActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_artwork);
    }

    @Override
    public void onBackPressed() {
        finish();
        Intent intent = new Intent(UploadArtworkActivity.this, MainActivity.class);
        startActivity(intent);
    }
}
